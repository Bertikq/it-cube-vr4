﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    private void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        transform.Rotate(new Vector3(-vertical, 0, 0) * Time.deltaTime * 50);
    }
}
